import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthorizationService} from '../authorization.service';
import {TokenStorageService} from '../token-storage.service';
import {LoginForm} from '../loginForm';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {RegistrationComponent} from '../registration/registration';

@Component({
  selector: 'app-login',
  templateUrl: './login.html',
  styleUrls: ['./login.css']
})
export class LoginComponent implements OnInit {

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  form: FormGroup;
  roles: string;
  private loginForm: LoginForm;
  hide: false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private authorizationService: AuthorizationService,
    private token: TokenStorageService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    if (this.token.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.token.getAuthorities();
    }
  }

  onSubmit() {
    console.log(this.form);

    this.loginForm = new LoginForm(
      this.form.get('username').value,
      this.form.get('password').value
    );

    console.log(this.loginForm);

    this.authorizationService.attemptAuth(this.loginForm).subscribe(data => {
        this.token.saveToken(data.token);
        this.token.saveUsername(data.username);
        this.token.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.token.getAuthorities();
        this.reloadPage();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isLoginFailed = true;
      });
  }

  registerPage() {
    this.dialog.closeAll();
    this.dialog.open(RegistrationComponent);
  }

  reloadPage() {
    window.location.reload();
  }
}
