import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './login/login';
import {RegistrationComponent} from './registration/registration';

@NgModule({
  imports: [BrowserModule],
  declarations: [CommonModule,
                FormsModule,
                LoginComponent,
                RegistrationComponent],
  providers: []
})
export class AuthorizationModule { }
