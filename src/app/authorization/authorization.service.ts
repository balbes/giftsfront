import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoginForm} from './loginForm';
import {Observable} from 'rxjs';
import {JwtResponse} from './jwt-response';
import {RegistrationForm} from './registrationForm';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AuthorizationService {
  private loginUrl = '/api/login';
  private registrationUrl = '/api/register';

  constructor(
    private http: HttpClient
  ) {
  }

  attemptAuth(credentials: LoginForm): Observable<JwtResponse> {
    console.log('yes');
    return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
  }

  registration(info: RegistrationForm): Observable<string> {
    return this.http.post<string>(this.registrationUrl, info, httpOptions);
  }
}
