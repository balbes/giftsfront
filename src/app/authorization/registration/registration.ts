import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RegistrationForm} from '../registrationForm';
import {AuthorizationService} from '../authorization.service';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.html',
  styleUrls: ['./registration.css']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;
  registerInfo: RegistrationForm;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private authorizationService: AuthorizationService,
    private dialog: MatDialog
  ) {
    this.form = fb.group({
      username: ['', Validators.required],
      passwordGroup: fb.group({
        password: ['', Validators.minLength(5)],
        passwordConfirm: ['', Validators.minLength(5)]
      }),
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.registerInfo = new RegistrationForm(
      this.form.get('username').value,
      this.form.get('passwordGroup.password').value
    );

    this.authorizationService.registration(this.registerInfo).subscribe(data => {
        console.log(data);
        this.isSignedUp = true;
        this.isSignUpFailed = false;
        this.dialog.closeAll();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
