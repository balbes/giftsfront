import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {UserService} from '../../services/user.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ChangePasswordForm} from '../change-password.form';
import {TokenStorageService} from '../token-storage.service';

@Component({
  templateUrl: './change-password.html',
  styleUrls: ['/change-password.css']
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup;
  private changePasswordForm: ChangePasswordForm;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService,
    private fb: FormBuilder,
    private token: TokenStorageService
  ) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      oldPassword: [''],
      newPassword: [''],
      confirmPassword: ['']
    });
  }

  onSubmit() {
    this.changePasswordForm = new ChangePasswordForm(
      this.token.getUsername(),
      this.form.get('oldPassword').value,
      this.form.get('newPassword').value,
    );

    this.userService.passwordChange(this.changePasswordForm).subscribe(
      data => {
        console.log(data);
        this.reloadPage();
      },
      error => {
        console.log(error);
      }
    );
  }

  reloadPage() {
    window.location.reload();
  }
}

