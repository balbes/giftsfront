import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../../services/order.service';

@Component({
  templateUrl: './users-delivered-orders.html',
  styleUrls: ['./users-delivered-orders.css']
})
export class UsersDeliveredOrdersComponent implements OnInit {

  usersOrders: any;
  displayedColumns = ['userFio', 'userNumber', 'userAddress', 'id', 'order', 'time', 'status', 'price'];

  constructor(
    private orderService: OrderService
  ) {
  }

  ngOnInit() {
    this.orderService.getUserDeliveredOrders().subscribe(data => {
      this.usersOrders = data;
    });
  }
}
