import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../../services/order.service';

@Component({
  templateUrl: './users-submited-orders.html',
  styleUrls: ['./users-submited-orders.css']
})
export class UsersSubmitedOrdersComponent implements OnInit {

  usersOrders: any;
  displayedColumns = ['userFio', 'userNumber', 'order', 'time', 'status', 'price', 'tools'];

  constructor(
    private orderService: OrderService
  ) {
  }

  ngOnInit() {
    this.orderService.getUsersSubmitedOrders().subscribe(data => {
      this.usersOrders = data;
    });

  }

  deliver(order: any) {
    this.orderService.deliverOrder(order.id).subscribe(() => {
      window.location.reload();
    });
  }

}
