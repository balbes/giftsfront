import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../../services/order.service';

@Component({
  templateUrl: './users-orders.html',
  styleUrls: ['./users-orders.css']
})
export class UsersOrdersComponent implements OnInit {

  usersOrders: any;
  displayedColumns = ['userFio', 'userNumber', 'userAddress', 'id', 'order', 'time', 'status', 'price', 'tools'];

  constructor(
    private orderService: OrderService
  ) {
  }

  ngOnInit() {
    this.orderService.getUsersOrders().subscribe(data => {
      this.usersOrders = data;
    });

  }

  submit(order: any) {
    this.orderService.submitOrder(order.id).subscribe(() => {
      window.location.reload();
    });
  }

}
