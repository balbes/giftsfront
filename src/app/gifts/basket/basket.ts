import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {BasketService} from '../../services/basket.service';
import {EmptyBasketComponent} from '../events/empty-basket/empty-basket';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SubmitOrderComponent} from '../events/submit-order/submit-order';
import {OrderService} from '../../services/order.service';
import {TokenStorageService} from '../../authorization/token-storage.service';

@Component({
  templateUrl: './basket.html',
  styleUrls: ['./basket.css']
})
export class BasketComponent implements OnInit {

  basketGifts: Array<any> = new Array();
  displayedColumns = ['order', 'price', 'tools'];
  totalPrice: any;
  orderInf = false;
  form: FormGroup;
  giftId: Array<any> = new Array<any>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog,
    private basket: BasketService,
    private fb: FormBuilder,
    private orderService: OrderService,
    private token: TokenStorageService
  ) {
  }

  ngOnInit() {
    this.basket.getBasket().forEach(data => {
      this.basketGifts.push(data);
      this.totalPrice = this.basket.createTotalPrice();
    });
    if (this.basketGifts.length === 0) {
      this.dialog.closeAll();
      this.dialog.open(EmptyBasketComponent);
    }

    this.form = this.fb.group({
      fio: [],
      numberPhone: [],
      address: []
    });
    console.log(this.basketGifts);
  }

  cleanBasket() {
    this.basket.cleanBasket();
    this.dialog.closeAll();
    console.log(this.totalPrice);

  }

  deleteFromBasket(gift: any) {
    this.basket.removeFromBasket(gift);
    this.refresh();
    this.dialog.closeAll();
    this.dialog.open(BasketComponent);
    console.log(this.totalPrice);

  }

  refresh() {
    this.basket.getBasket().forEach(data => {
      this.basketGifts.push(data);
    });
    this.totalPrice = this.basket.createTotalPrice();
    console.log(this.totalPrice);

  }

  submitOrder() {
    this.basketGifts.forEach(data => {
      this.giftId.push(data.id);
      console.log(this.totalPrice);

    });

    this.orderService.makeOrder(
      {
        customerAddres: this.form.get('address').value,
        customerNumber: this.form.get('numberPhone').value,
        customerFio: this.form.get('fio').value,
        username: this.token.getUsername(),
        price: this.basket.totalPrice,
        giftsId: this.giftId
      }
    ).subscribe(() => {
      this.dialog.closeAll();
      this.dialog.open(SubmitOrderComponent);
      this.basket.cleanBasket();
    });
  }

}
