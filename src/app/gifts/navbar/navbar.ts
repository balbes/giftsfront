import {Component, Input, OnInit} from '@angular/core';
import {TokenStorageService} from '../../authorization/token-storage.service';
import {MatDialog} from '@angular/material';
import {LoginComponent} from '../../authorization/login/login';
import {ChangePasswordComponent} from '../../authorization/change-password/change-password';
import {MakeUserGiftComponent} from '../user/make-user-gift/make-user-gift';
import {BasketComponent} from '../basket/basket';
import {MakeNewGiftComponent} from '../creator/make-new-gift/make-new-gift';


@Component({
  selector: 'app-pizza-navbar',
  templateUrl: './navbar.html',
  styleUrls: ['./navbar.css']
})
export class NavbarComponent implements OnInit {

  info: any;

  constructor(
    private token: TokenStorageService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {

    this.info = {
      name: this.token.getUsername(),
      role: this.token.getAuthorities()
    };

  }

  openEnterDialog() {
    this.dialog.open(LoginComponent);
  }

  changePasswordDialog() {
    this.dialog.open(ChangePasswordComponent);
  }

  toBasket() {
    this.dialog.open(BasketComponent);
  }

  openMakeGiftDialog() {
    this.dialog.open(MakeUserGiftComponent);
  }

  addNewGift() {
    this.dialog.open(MakeNewGiftComponent);
  }

  logout() {
    this.token.signOut();
    window.location.reload();
  }
}
