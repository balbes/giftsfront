import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {GiftsService} from '../../../services/gifts.service';
import {BasketService} from '../../../services/basket.service';
import {OnTheBasketComponent} from '../../events/on-the-basket/on-the-basket';
import {ComponentService} from '../../../services/component.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CreateNewGiftComponent} from '../../events/create-new-gift/create-new-gift';

@Component({
  templateUrl: './make-new-gift.html',
  styleUrls: ['./make-new-gift.css']
})
export class MakeNewGiftComponent implements OnInit {

  ingredients: any;
  boxType: any;
  chosenIngredients: Array<any> = new Array<any>();
  chosenIngredient: Array<any> = new Array<any>();
  price = 0;
  title = '';
  ingredientId: Array<any> = new Array<any>();
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private giftService: GiftsService,
    private dialog: MatDialog,
    private basket: BasketService,
    private component: ComponentService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.component.getComponents().subscribe(data => {
      this.ingredients = data;
    });

    this.form = this.fb.group(
      {
        title: [],
        price: []
      }
    );
    // this.ingredients = [
    //   {
    //     id: 1,
    //     title: 'красная',
    //     type: 'Коробка',
    //     price: 2
    //   },
    //   {
    //     id: 2,
    //     title: 'синяя',
    //     type: 'Коробка',
    //     price: 2
    //   },
    //   {
    //     id: 3,
    //     title: 'зеленая',
    //     type: 'Коробка',
    //     price: 2
    //   },
    //   {
    //     id: 4,
    //     title: 'хрень',
    //     type: 'цйу',
    //     price: 2
    //   },
    //   {
    //     id: 5,
    //     title: 'что-то',
    //     type: 'цйуйц',
    //     price: 2
    //   },
    //   {
    //     id: 6,
    //     title: 'еще что-то',
    //     type: 'цйуцу',
    //     price: 2
    //   },
    //   {
    //     id: 7,
    //     title: 'хз что',
    //     type: 'йцуйц',
    //     price: 2
    //   }
    // ];
  }

  selectBox(ingredient: any) {
    this.boxType = ingredient;
    console.log(this.boxType);
  }

  addIngredients(ingred: any, exist: boolean) {
    this.chosenIngredient = [];

    this.chosenIngredients.forEach(
      ingredient => {
        if (ingredient.title !== ingred.title) {
          this.chosenIngredient.push(ingredient);
        } else {
          exist = true;
        }
      }
    );
    if (!exist) {
      this.chosenIngredient.push(ingred);
    }

    this.chosenIngredients = this.chosenIngredient;

    console.log(this.chosenIngredients);
  }

  addGift() {
    this.chosenIngredients.push(this.boxType);
    this.chosenIngredients.forEach(data => {
      this.ingredientId.push(data.id);
    });
    this.giftService.addNewGift(
      {
        ingredientsId: this.ingredientId,
        price: this.form.get('price').value,
        title: this.form.get('title').value
      }
    ).subscribe(() => {
      this.dialog.open(CreateNewGiftComponent);
    });
  }


}
