import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {GiftsService} from '../../../services/gifts.service';
import {BasketService} from '../../../services/basket.service';
import {OnTheBasketComponent} from '../../events/on-the-basket/on-the-basket';
import {TokenStorageService} from '../../../authorization/token-storage.service';
import {ToYourGiftsComponent} from '../../events/to-your-gifts/to-your-gifts';
import {ComponentService} from '../../../services/component.service';

@Component({
  templateUrl: './make-user-gift.html',
  styleUrls: ['./make-user-gift.css']
})
export class MakeUserGiftComponent implements OnInit {

  ingredients: any;
  boxType: any;
  chosenIngredients: Array<any> = [];
  chosenIngredient: Array<any> = [];
  ingredientsId: Array<any> = new Array<any>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private giftService: GiftsService,
    private dialog: MatDialog,
    private basket: BasketService,
    private token: TokenStorageService,
    private component: ComponentService
  ) {
  }

  ngOnInit() {
    this.component.getComponents().subscribe(data => {
      this.ingredients = data;
    });
    // this.ingredients = [
    //   {
    //     id: '1',
    //     title: 'красная',
    //     type: 'Коробка',
    //     price: 2
    //   },
    //   {
    //     id: '2',
    //     title: 'синяя',
    //     type: 'Коробка',
    //     price: 2
    //   },
    //   {
    //     id: '3',
    //     title: 'зеленая',
    //     type: 'Коробка',
    //     price: 2
    //   },
    //   {
    //     id: '4',
    //     title: 'хрень',
    //     type: 'цйу',
    //     price: 2
    //   },
    //   {
    //     id: '5',
    //     title: 'что-то',
    //     type: 'цйуйц',
    //     price: 2
    //   },
    //   {
    //     id: '6',
    //     title: 'еще что-то',
    //     type: 'цйуцу',
    //     price: 2
    //   },
    //   {
    //     id: '7',
    //     title: 'хз что',
    //     type: 'йцуйц',
    //     price: 2
    //   }
    // ];
  }

  selectBox(ingredient: any) {
    this.boxType = ingredient;
    console.log(this.boxType);
  }

  addIngredients(ingred: any, exist: boolean) {
    this.chosenIngredient = [];

    this.chosenIngredients.forEach(
      ingredient => {
        if (ingredient.title !== ingred.title) {
          this.chosenIngredient.push(ingredient);
        } else {
          exist = true;
        }
      }
    );
    if (!exist) {
      this.chosenIngredient.push(ingred);
    }

    this.chosenIngredients = this.chosenIngredient;

    console.log(this.chosenIngredients);
  }

  toBasket() {
    this.chosenIngredients.push(this.boxType);
    this.chosenIngredients.forEach(data => {
      this.ingredientsId.push(data.id);
    });

    this.giftService.newUserGift(
      {
        ingredientsId: this.ingredientsId,
        username: this.token.getUsername()
  }).
    subscribe(() => {
      this.dialog.closeAll();
      this.dialog.open(ToYourGiftsComponent);
    });
  }

}
