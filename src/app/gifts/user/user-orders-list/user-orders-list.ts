import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../../services/order.service';
import {TokenStorageService} from '../../../authorization/token-storage.service';

@Component({
  templateUrl: './user-orders-list.html',
  styleUrls: ['./user-orders-list.css']
})
export class UserOrdersListComponent implements OnInit {

  orderList: any;
  displayedColumns = ['id', 'order', 'time', 'status', 'price'];

  constructor(
    private orderService: OrderService,
    private token: TokenStorageService
  ) {
  }

  ngOnInit() {
    this.orderService.getMyOrders(this.token.getUsername()).subscribe(data => {
      this.orderList = data;
    });

    // this.orderList = [
    //   {
    //     id: 1,
    //     order: 'что-то1',
    //     time: '12',
    //     status: '23',
    //     price: 21
    //   },
    //   {
    //     id: 2,
    //     order: 'что-то2',
    //     time: '12',
    //     status: '23',
    //     price: 21
    //   },
    //   {
    //     id: 3,
    //     order: 'что-то3',
    //     time: '12',
    //     status: '23',
    //     price: 21
    //   }
    // ];
  }

}
