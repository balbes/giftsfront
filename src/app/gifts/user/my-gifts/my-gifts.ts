import {Component, OnInit} from '@angular/core';
import {GiftsService} from '../../../services/gifts.service';
import {BasketService} from '../../../services/basket.service';
import {MatDialog} from '@angular/material';
import {OnTheBasketComponent} from '../../events/on-the-basket/on-the-basket';
import {TokenStorageService} from '../../../authorization/token-storage.service';

@Component({
  templateUrl: './my-gifts.html',
  styleUrls: ['./my-gifts.css']
})
export class MyGiftsComponent implements OnInit {

  gifts: any;

  constructor(
    private giftsService: GiftsService,
    private basket: BasketService,
    private dialog: MatDialog,
    private token: TokenStorageService
  ) {
  }

  ngOnInit() {
    this.giftsService.getMyGifts(this.token.getUsername()).subscribe(data => {
      this.gifts = data;
    });
  }

  toBasket(gift: any) {
    this.basket.addToBasket(gift);
    this.dialog.open(OnTheBasketComponent);
    console.log(gift);
  }
}
