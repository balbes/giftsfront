import {Component, OnInit} from '@angular/core';
import {GiftsService} from '../../services/gifts.service';
import {BasketService} from '../../services/basket.service';
import {MatDialog} from '@angular/material';
import {OnTheBasketComponent} from '../events/on-the-basket/on-the-basket';

@Component({
  selector: 'app-main',
  templateUrl: './main.html',
  styleUrls: ['./main.css']
})
export class MainComponent implements OnInit {

  gifts: any;

  constructor(
    private giftsService: GiftsService,
    private basket: BasketService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.giftsService.getDefaultGifts().subscribe(data => {
      this.gifts = data;
      console.log(data);
      console.log(this.gifts);
    });

  //   this.gifts = [
  //     {
  //       id: 1,
  //       title: 'sdasd',
  //       components: [
  //         {
  //           id: '',
  //           title: '',
  //         }
  //       ],
  //       description: 'asdasdsad',
  //       price: 12
  //     },
  //     {
  //       id: 2,
  //       title: 'sdasd',
  //       components: [
  //         {
  //           id: '',
  //           title: '',
  //         }
  //       ],
  //       description: 'asdasdsad',
  //       price: 12
  //     },
  //     {
  //       id: 3,
  //       title: 'sdasd',
  //       components: [
  //         {
  //           id: '',
  //           title: '',
  //         }
  //       ],
  //       description: 'asdasdsad',
  //       price: 12
  //     },
  //     {
  //       id: 4,
  //       title: 'sdasd',
  //       description: 'asdasdsad',
  //       price: 12
  //     },
  //     {
  //       id: 5,
  //       title: 'sdasd',
  //       components: [
  //         {
  //           id: '',
  //           title: '',
  //         }
  //       ],
  //       description: 'asdasdsad',
  //       price: 12
  //     },
  //     {
  //       id: 6,
  //       title: 'sdasd',
  //       components: [
  //         {
  //           id: '',
  //           title: '',
  //         }
  //       ],
  //       description: 'asdasdsad',
  //       price: 12
  //     }
  //   ];
  }

  toBasket(gift: any) {
    this.basket.addToBasket(gift);
    this.dialog.open(OnTheBasketComponent);
    console.log(gift);
  }
}
