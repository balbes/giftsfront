import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';

@Component({
  templateUrl: './empty-basket.html',
  styleUrls: ['./empty-basket.css']
})
export class EmptyBasketComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.dialog.closeAll();
    }, 700);
  }

}
