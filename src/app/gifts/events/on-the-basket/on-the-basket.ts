import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';

@Component({
  templateUrl: './on-the-basket.html',
  styleUrls: ['./on-the-basket.css']
})
export class OnTheBasketComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.dialog.closeAll();
    }, 700);
}

}
