import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';

@Component({
  templateUrl: './submit-order.html',
  styleUrls: ['./submit-order.css']
})
export class SubmitOrderComponent implements OnInit{

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
  }

}
