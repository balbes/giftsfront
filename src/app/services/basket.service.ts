import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class BasketService {

  basketGifts: Set<any> = new Set();
  totalPrice = 0;

  constructor() {
  }

  getBasket() {
    return this.basketGifts;
  }

  addToBasket(gift: any) {
    this.basketGifts.add(gift);
    this.createTotalPrice();
  }

  removeFromBasket(gift: any) {
    this.basketGifts.delete(gift);
    this.createTotalPrice();
  }

  cleanBasket() {
    this.basketGifts.clear();
    this.totalPrice = 0;
  }

  createTotalPrice() {
    this.basketGifts.forEach(gift => {
      this.totalPrice += gift.price;
    });
  }

}
