import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class OrderService {

  constructor(
    private http: HttpClient
  ) {
  }

  makeOrder(form: any): Observable<any> {
    return this.http.post('api/orders/makeOrder', form);
  }

  getMyOrders(username: string): Observable<any> {
    return this.http.get('api/orders/getMy/' + username);
  }

  getUsersOrders(): Observable<any> {
    return this.http.get('api/orders/getUsersOrders');
  }

  submitOrder(form: any): Observable<any> {
    return this.http.post('api/orders/submitOrder', form);
  }

  getUsersSubmitedOrders(): Observable<any> {
    return this.http.get('api/orders/getUsersSubmitedOrders');
  }

  deliverOrder(form: any): Observable<any> {
    return this.http.post('api/orders/deliverOrder', form);
  }

  getUserDeliveredOrders(): Observable<any> {
    return this.http.get('api/orders/getUsersDeliveredOrders');
  }

}
