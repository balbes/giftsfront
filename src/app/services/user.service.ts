import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ChangePasswordForm} from '../authorization/change-password.form';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  passwordChange(changePasswordForm: ChangePasswordForm): Observable<any> {
    return this.http.post('/api/user/changePassword', changePasswordForm, httpOptions);
  }
}
