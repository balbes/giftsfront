import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class GiftsService {

  constructor(
    private http: HttpClient
  ) {
  }

  getDefaultGifts(): Observable<any> {
    return this.http.get('api/gifts/getDefaultGifts');
  }

  newUserGift(form: any): Observable<any> {
    return this.http.post('api/gifts/newUserGift', form);
  }

  addNewGift(form: any): Observable<any> {
    return this.http.post('api/gifts/addNewGift', form);
  }

  getMyGifts(username: string): Observable<any> {
    return this.http.get('api/gifts/' + username);
  }
}
