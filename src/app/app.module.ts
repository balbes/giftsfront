import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {MaterialModule} from './material.module';
import {LoginComponent} from './authorization/login/login';
import {ChangePasswordComponent} from './authorization/change-password/change-password';
import {RegistrationComponent} from './authorization/registration/registration';
import {AuthorizationService} from './authorization/authorization.service';
import {TokenStorageService} from './authorization/token-storage.service';
import {MainComponent} from './gifts/main/main';
import {GiftsService} from './services/gifts.service';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {NavbarComponent} from './gifts/navbar/navbar';
import {BasketComponent} from './gifts/basket/basket';
import {BasketService} from './services/basket.service';
import {UserOrdersListComponent} from './gifts/user/user-orders-list/user-orders-list';
import {MakeUserGiftComponent} from './gifts/user/make-user-gift/make-user-gift';
import {OnTheBasketComponent} from './gifts/events/on-the-basket/on-the-basket';
import {RouterModule, Routes} from '@angular/router';
import {UsersOrdersComponent} from './gifts/administrator/users-orders/users-orders';
import {MakeNewGiftComponent} from './gifts/creator/make-new-gift/make-new-gift';
import {OrderService} from './services/order.service';
import {ComponentService} from './services/component.service';
import {UsersSubmitedOrdersComponent} from './gifts/administrator/users-submited-orders/users-submited-orders';
import {EmptyBasketComponent} from './gifts/events/empty-basket/empty-basket';
import {SubmitOrderComponent} from './gifts/events/submit-order/submit-order';
import {ToYourGiftsComponent} from './gifts/events/to-your-gifts/to-your-gifts';
import {UsersDeliveredOrdersComponent} from './gifts/administrator/users-delivered-orders/users-delivered-orders';
import {CreateNewGiftComponent} from './gifts/events/create-new-gift/create-new-gift';
import {MyGiftsComponent} from './gifts/user/my-gifts/my-gifts';

const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'myOrders',
    component: UserOrdersListComponent
  },
  {
    path: 'usersOrders',
    component: UsersOrdersComponent
  },
  {
    path: 'myGifts',
    component: MyGiftsComponent
  },
  {
    path: 'unsubmitedOrders',
    component: UsersOrdersComponent
  },
  {
    path: 'submitedOrders',
    component: UsersSubmitedOrdersComponent
  },
  {
    path: 'deliveredOrders',
    component: UsersDeliveredOrdersComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChangePasswordComponent,
    RegistrationComponent,
    MainComponent,
    NavbarComponent,
    BasketComponent,
    UserOrdersListComponent,
    MakeUserGiftComponent,
    OnTheBasketComponent,
    UsersOrdersComponent,
    MakeNewGiftComponent,
    UsersSubmitedOrdersComponent,
    EmptyBasketComponent,
    SubmitOrderComponent,
    ToYourGiftsComponent,
    UsersDeliveredOrdersComponent,
    CreateNewGiftComponent,
    MyGiftsComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthorizationService,
    TokenStorageService,
    GiftsService,
    BasketService,
    OrderService,
    ComponentService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginComponent,
    ChangePasswordComponent,
    RegistrationComponent,
    BasketComponent,
    OnTheBasketComponent,
    MakeUserGiftComponent,
    EmptyBasketComponent,
    SubmitOrderComponent,
    MakeNewGiftComponent,
    ToYourGiftsComponent,
    CreateNewGiftComponent
  ]
})
export class AppModule { }
